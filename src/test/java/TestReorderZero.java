import org.junit.Test;

import java.util.Arrays;
import java.util.Random;

import static ZeroInArrays2theEnd.ReorderOne.reorderOne;
import static ZeroInArrays2theEnd.ReorderBubble.reorderBubble;
import static ZeroInArrays2theEnd.ReorderTwo.partition;


public class TestReorderZero {
    @Test
    public void testReorderOne() {
        int[] A = {6, 0, 8, 2, 3, 0, 4, 0, 1};
        reorderOne(A);
        System.out.println(Arrays.toString(A));

    }

    @Test
    public void testReorderOneInRandom20() {
        Random random = new Random();
        int[] array20;
        {
            array20 = new int[20];
            for (int i = 0; i < array20.length; i++) {
                array20[i] = random.nextInt(5);
            }
        }
        System.out.println(Arrays.toString(array20));
        reorderOne(array20);
        System.out.println(Arrays.toString(array20));
    }

    @Test
    public void testReorderTwo() {
        int[] A = {6, 0, 8, 2, 3, 0, 4, 0, 1};
        partition(A);
        System.out.println(Arrays.toString(A));
    }

    @Test
    public void testReorderTwoInRandom20() {
        Random random = new Random();
        int[] array20;
        {
            array20 = new int[20];
            for (int i = 0; i < array20.length; i++) {
                array20[i] = random.nextInt(5);
            }
        }
        System.out.println(Arrays.toString(array20));
        partition(array20);
        System.out.println(Arrays.toString(array20));
    }

    @Test
    public void testReorderBubble() {
        int[] mas = {34, 0, 0, 14, 15, 0, 18, 0, 0, 1, 20};
        reorderBubble(mas);
        System.out.println(Arrays.toString(mas));
    }

    @Test
    public void testReorderBubbleInRandom20() {
        Random random = new Random();
        int[] array20;
        {
            array20 = new int[20];
            for (int i = 0; i < array20.length; i++) {
                array20[i] = random.nextInt(5);
            }
        }
        System.out.println(Arrays.toString(array20));
        reorderBubble(array20);
        System.out.println(Arrays.toString(array20));
    }
}
