package ZeroInArrays2theEnd;

import java.util.Arrays;

public class ReorderOne {
    public static void reorderOne(int[] A) {
        int freeIndex = 0;

        for (int i : A) {
            if (i != 0) {
                A[freeIndex++] = i;
            }
        }

        for (int i = freeIndex; i < A.length; i++) {
            A[i] = 0;
        }
    }

    public static void main(String[] args) {

    }
}


