package ZeroInArrays2theEnd;

import java.util.Arrays;

public class ReorderBubble {

    public static void reorderBubble(int [] mas) {
        int isSorted = 0;
        int buf;
        while (isSorted< mas.length* mas.length) {
            for (int i = 0; i < mas.length - 1; i++) {
                if (mas[i] == 0) {
                    buf = mas[i];
                    mas[i] = mas[i + 1];
                    mas[i + 1] = buf;
                    isSorted +=1;
                }
            }
        }
    }
}

