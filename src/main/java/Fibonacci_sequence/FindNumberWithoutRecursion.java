package Fibonacci_sequence;
/*
Реализовать два метода нахождения числа Фибоначчи. Оба метода принимают на вход число,
 которое обозначает порядковый номер числа Фибоначчи. Результат работы обоих методов -
это число Фибоначчи, которое находится под данным номером.
Пример:
Ввели число 3 - метод вернет число 2
Ввели числ 5 - метод вернет число 5
Ввели число 10 - метод вернет число 55
 */

import java.util.InputMismatchException;
import java.util.Scanner;

public class FindNumberWithoutRecursion {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("What number of Fibonacci sequence you want to know ");

        int inputNumber = scanner.nextInt() +1;
        if ((inputNumber) > 49 || (inputNumber) < 1) {
            System.out.println("Be prudent, please");
        } else {
            int[] fibonacciSeqArray;
            fibonacciSeqArray = new int[inputNumber];
            fibonacciSeqArray[0] = 0;
            fibonacciSeqArray[1] = 1;
            for (int i = 2; i < fibonacciSeqArray.length; i++) {
                fibonacciSeqArray[i] = fibonacciSeqArray[i - 1] + fibonacciSeqArray[i - 2];
            }
            System.out.println(fibonacciSeqArray[fibonacciSeqArray.length -1]);
        }
    }
}
