package Fibonacci_sequence;

import java.util.Scanner;

public class FindNumberByRecursion {
    static int fiboyaichi(int n){

        if (n == 0){
            return 0;
        }
        if (n == 1){
            return 1;
        }
        else{
            return fiboyaichi(n - 1) + fiboyaichi(n - 2);
        }
    }

    public static void main(String[] args) {
        System.out.println("What number of Fibonacci sequence you want to know ");
        Scanner sc = new Scanner(System.in);
        int answer = fiboyaichi(sc.nextInt());
        System.out.println(answer);
    }
}
